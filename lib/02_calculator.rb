def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array)
  array.reduce(0) {|sum, num| sum + num}
end

#Bonus

def multiply(num1, num2)
  num1 * num2
end

def power(num1, num2)
  num1**num2
end

def factorial(num)
  if num < 2
    result = 1
  else
    result = (2..num).to_a.reduce(:*)
  end
  result
end
