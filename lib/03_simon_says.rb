def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, number = 2)
  repeated_str = []
  number.times {repeated_str << str}
  repeated_str.join(' ')
end

def start_of_word(str, number)
  str[0, number]
end

def first_word(str)
  str.split[0]
end

def titleize(str)
  words = str.split
  words.each_with_index do |word, i|
    unless little_word?(word) && i != 0
      word.capitalize!
    end
  end
  words.join(' ')
end

def little_word?(word)
  ['a', 'an', 'the', 'at', 'by', 'for', 'in', 'of', 'on',
   'to', 'up', 'and', 'as', 'but', 'or', 'nor', 'over'].include?(word)
end
