def translate(str)
  words = str.split
  words.each_with_index do |word, idx|
    words[idx] = pig_latin(word)
  end
  words.join(' ')
end

def pig_latin(word)
  letters = word.split('')
  until vowel?(letters[0])
    letters.rotate!
    if letters[0] == 'u' && letters[-1] == 'q'
      letters.rotate!
    end
  end
  letters.join + 'ay'
end

def vowel?(letter)
  ['a', 'e', 'i', 'o', 'u'].include?(letter)
end
